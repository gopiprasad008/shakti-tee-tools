#! /bin/bash
#
# Script to build RISC-V ISA simulator, proxy kernel, and GNU toolchain.
# Tools will be installed to $RISCV.

source build.common

check_version() {
    $1 --version | awk "NR==1 {if (\$NF>$2) {exit 0} exit 1}" || (
        echo $3 requires at least version $2 of $1. Aborting.
        exit 1
    )
}


check_version automake 1.14 "OpenOCD build"
check_version autoconf 2.64 "OpenOCD build"

configure_project openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror
configure_project fesvr --prefix=$RISCV
# needed for the rest
build_project fesvr
install_project fesvr

configure_project isa-sim --prefix=$RISCV --with-fesvr=$RISCV
configure_project gnu-toolchain --prefix=$RISCV --with-cmodel=medany

build_project openocd
install_project openocd

build_project isa-sim
install_project isa-sim

build_project gnu-toolchain
install_project gnu-toolchain

#XXX KC: Does `make linux` build newlib gcc as well? In which case the former
#is not necessary.
build_project gnu-toolchain linux
install_project gnu-toolchain
